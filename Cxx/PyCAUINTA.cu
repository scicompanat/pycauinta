
#include "PyCAUINTA.h"
#include <pycaUtils.h>
#include <MemOpers.h>

// hard-code window and patch radius for now
#define WINRAD_X 10
#define WINRAD_Y 10
#define WINRAD_Z 0
#define PATCHRAD_X 2
#define PATCHRAD_Y 2
#define PATCHRAD_Z 0

#include "PyCAUINTAKernel.cu"

namespace PyCAUINTA {

void 
PyCAUINTA::
Denoise(PyCA::Image3D& d_o, 
	const PyCA::Image3D& d_i, 
	float kSig)
{
    MK_CHECK2_ALL(d_o, d_i);

    const PyCA::Vec3Di &sz = d_o.size();

    // set up grid and threads
    dim3 threads(16,16);
    dim3 grids(PyCA::iDivUp(sz.x, threads.x), 
	       PyCA::iDivUp(sz.y, threads.y));

    // call the kernel
    UINTA_Kernel<<<grids, threads, 0, 0>>>
	(d_i.get(), d_o.get(), kSig,
	 sz.x, sz.y, 0);
}

} // end namespace PyCAUINTA
