#ifndef __PyCAUINTA_H__
#define __PyCAUINTA_H__

#include <pycaConst.h>
#include <estream.h>
#include <Image3D.h>
#include <Field3D.h>
#include <Vec3D.h>


namespace PyCAUINTA {

class PyCAUINTA {
public:
    
    static void Denoise(PyCA::Image3D& d_o, 
			const PyCA::Image3D& d_i, 
			float kSig);
};

} // end namespace PyCAUINTA

#endif // __PyCAUINTA_H__
