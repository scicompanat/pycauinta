// This file is #include'd in PyCAUINTA.cxx

#include <cuda_runtime.h>
#include <device_functions.h>

#define MAX_LAYERS 3

#define RETURN_WEIGHTS_FALSE 0
#define RETURN_WEIGHTS_TRUE 1
#define USE_MASK_FALSE 0
#define USE_MASK_TRUE 1
#define USE_SELF_FALSE 0
#define USE_SELF_TRUE 1

typedef float FrameType;

// ================================================================
// Helper Functions
// ================================================================

template<class T> 
__device__
inline
T rtd_max(T a, T b)
{
    return a > b ? a : b;
}

template<class T> 
__device__
inline
T rtd_min(T a, T b)
{
    return a < b ? a : b;
}

inline __host__ __device__ float sqrDiff(float a, float b){
    return ((b - a) * (b - a));
}

template <class T> 
inline __host__ __device__ 
T make_out(float v)
{
    return static_cast<T>(v);
}

// ================================================================
// Standard UINTA implementation
// ================================================================

/**
 * Return clamped value at x,y,z from global memory dataset of size
 * sz_x,sz_y,sz_z
 */
static
inline
__device__
FrameType getVal(const FrameType *data, 
		 int x, int y, int z,
		 int sz_x, int sz_y, int sz_z)
{
  return data[sz_x*(rtd_min(rtd_max(z, 0), sz_z-1)*sz_y +
		    rtd_min(rtd_max(y, 0), sz_y-1)) +
	      rtd_min(rtd_max(x, 0), sz_x-1)];
}

/** Unchecked data setting */
static
inline
__device__
void setVal(FrameType *data, FrameType val,
	     int x, int y, int z,
	     int sz_x, int sz_y, int sz_z)
{
    data[sz_x*(z*sz_y + y) + x] = val;
}

/**
 * Compute sum-of-squared difference between patch centered at 
 * (x, y ,z) and patch centerd at (x+offx, y+offy, z+offz)
 */
template<int UseMask>
inline __device__
float PatchDiff(const FrameType *imdata,
		int imageW, int imageH,
		int x, int y, int z,
		int offx, int offy, int offz)
{
    uint maskIdx = 0;
    float weightIJK = 0;

    for(float n = -PATCHRAD_Z; n <= PATCHRAD_Z; n++){
	for(float m = -PATCHRAD_Y; m <= PATCHRAD_Y; m++){
	    for(float l = -PATCHRAD_X; l <= PATCHRAD_X; l++){
		float dist;
		dist = 
		    sqrDiff(getVal(imdata, 
				   x + offx + l, y + offy + m, z + offz + n,
				   imageW, imageH, 1),
			    getVal(imdata,     
				   x + l,     y + m,     z + n,
				   imageW, imageH, 1));
		
		if(UseMask){
		    //weightIJK += cdMask[maskIdx]*dist;
		}else{
		    weightIJK += dist;
		}
		
		maskIdx++;
	    }
	}
    }

    return weightIJK;
}

// ================================================================
// Standard UINTA implementation
// ================================================================

template<int ReturnWeights, int UseMask, int UseSelf>
inline __device__
FrameType UINTAFunc(const FrameType *imdata,
		    float kSig,
		    int imageW, int imageH,
		    int x, int y, int z)
{
    
    const float invSig2 = 1.0f/((float)(2.0f*kSig*kSig));
    
    //Total sum of pixel weights
    float sumWeights = 0;
    //Result accumulator
    float val = 0;
    
    //Cycle through UINTA window, surrounding (x, y) texel
    for(int k = -WINRAD_Z; k <= WINRAD_Z; k++){
	for(int j = -WINRAD_Y; j <= WINRAD_Y; j++){
	    for(int i = -WINRAD_X; i <= WINRAD_X; i++){

		if(!UseSelf){
		    // don't process center pixel
		    if(i == 0 && j == 0 && k == 0) continue;
		}
		
		// Find feature-space distance from (x, y, z) to 
		// (x + i, y + j, z + k)
		float weightIJK = 
		    PatchDiff<UseMask>(imdata,
				       imageW, imageH,
				       x, y, z, i, j, k);
	  
		// exponential falloff in feature-space distance
		weightIJK     = __expf( -(weightIJK * invSig2) );
	  
		if(!ReturnWeights){
		    //Accumulate (x + i, y + j, z + k) texel color
		    //with computed weight
		    float valIJK = getVal(imdata, 
					  x + i, y + j, z + k,
					  imageW, imageH, 1);
		    val += valIJK * weightIJK;
		}
	  
		//Sum of weights for color normalization to [0..1] range
		sumWeights  += weightIJK;

	    } // window x loop
	} // window y loop
    } // window z loop
  
    if(!ReturnWeights){
	//Normalize result color by sum of weights
	sumWeights = 1.0f / sumWeights;
	val *= sumWeights;
    
	//Original pixel value
	// float orig_val = tex3D(imdata, x, y, z, x + i, y + j, z + k);
	// val = (1.0-cdLambda)*val + cdLambda*orig_val;
    
	return make_out<FrameType>(val);
    }else{
	return make_out<FrameType>(sumWeights);
    }
}

// kernel definition must be wrapped in 'extern "C"' block to avoid
// c++ name mangling, so that the function can be found by pycuda
#ifdef PYCUDA
extern "C" {
#endif // PYCUDA

    __global__ void 
    UINTA_Kernel(const FrameType *imdata,
		 FrameType *update,
		 float kSig,
		 int imageW,
		 int imageH,
		 int bufferSliceIdx)
    {
	const int ix = blockDim.x * blockIdx.x + threadIdx.x;
	const int iy = blockDim.y * blockIdx.y + threadIdx.y;
	if(ix < imageW && iy < imageH){
	    update[imageW * iy + ix] = 
		UINTAFunc<RETURN_WEIGHTS_FALSE, 
			  USE_MASK_FALSE,
			  USE_SELF_TRUE>
		(imdata, kSig, imageW, imageH, ix, iy, 0);
	}
    }

#ifdef PYCUDA
} // end extern "C"
#endif // PYCUDA


