#
# This is an example of running the precompiled PyCAUINTA library.
# Make sure the bin/SWIG directory is on your PYTHONPATH so that
# PyCAUINTA.py and _PyCAUINTA.so can be found.
#

import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display
# there's a PyCAUINTA namespace and PyCAUINTA class, so the class is
# PyCAUINTA.PyCAUINTA
from PyCAUINTA import PyCAUINTA as uinta

import matplotlib.pyplot as plt

if __name__ == '__main__':

    plt.close('all')

    kSig = 0.2
    
    mType = ca.MEM_DEVICE

    im_orig = common.LoadPNGImage('../Data/lena.png', mType=mType)
    im_denoised = im_orig.copy()
    diff = im_orig.copy()

    uinta.Denoise(im_denoised, im_orig, kSig)

    ca.Sub(diff, im_orig, im_denoised)

    plt.figure('UINTA Test')
    plt.clf()
    plt.subplot(1,3,1)
    display.DispImage(im_orig, 'orig', newFig=False)
    plt.subplot(1,3,2)
    display.DispImage(im_denoised, 'denosied', newFig=False)
    plt.subplot(1,3,3)
    display.DispImage(diff, 'diff', newFig=False)
    plt.colorbar()
    plt.show()

