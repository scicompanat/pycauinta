#
# Example of running cuda kernel directly (no precompilation required)
# using pycuda
#

import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Display as display

import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

import numpy as np

import matplotlib.pyplot as plt
plt.ion() 

import os

# Initialize cuda kernels

CUDA_DEFS = \
"""
#define WINRAD_X {winRad[0]}
#define WINRAD_Y {winRad[1]}
#define WINRAD_Z {winRad[2]}
#define PATCHRAD_X {patchRad[0]}
#define PATCHRAD_Y {patchRad[1]}
#define PATCHRAD_Z {patchRad[2]}
"""

cublock = (8,8,1)

if __name__ == '__main__':

    # settings 
    patchRad=[2,2,0]
    winRad=[10,10,0]
    kSig=0.2

    # read in cuda kernel, and add some definitions to the top
    cudafilename = '../Kernel/PyCAUINTAKernel.cu'
    cudafile = open(cudafilename)
    defstring = CUDA_DEFS.format(winRad=winRad, patchRad=patchRad)
    print defstring
    cudafile_text = defstring 
    cudafile_text += cudafile.read()

    # compile the kernel
    mod = SourceModule(cudafile_text, no_extern_c=True,
                       options=['-DPYCUDA'])

    # find the kernel
    PyCAUINTA_Kernel = mod.get_function("UINTA_Kernel")

    mType = ca.MEM_DEVICE

    im_orig = common.LoadPNGImage('../Data/lena.png', mType=mType)
    im_denoised = im_orig.copy()
    diff = im_orig.copy()
    # parameters are 32 bit
    sz = np.array(im_orig.size().tolist(), dtype='int32')

    Imblock = (8,8,1)
    Imgrid = (int(np.ceil(float(im_orig.size().x/Imblock[0]))),
              int(np.ceil(float(im_orig.size().y/Imblock[1]))))

    # pointers are 64 bit on 64 bit architectures
    inaddr = np.uint64(im_orig.rawptr())
    outaddr = np.uint64(im_denoised.rawptr())
    print hex(inaddr)
    print hex(outaddr)
    PyCAUINTA_Kernel(inaddr, outaddr,
                     np.float32(kSig),
                     sz[0], sz[1], np.int32(0),
                     block=Imblock, grid=Imgrid)

    ca.Sub(diff, im_orig, im_denoised)

    plt.figure('UINTA Test')
    plt.clf()
    plt.subplot(1,3,1)
    display.DispImage(im_orig, 'orig', newFig=False)
    plt.subplot(1,3,2)
    display.DispImage(im_denoised, 'denosied', newFig=False)
    plt.subplot(1,3,3)
    display.DispImage(diff, 'diff', newFig=False)
    plt.colorbar()
    plt.show()
