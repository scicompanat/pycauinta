This project is an example of extending the PyCA library.  It
implements GPU-based UINTA (NL-means) denoising.  There are two ways
of extending the library -- compiling an extension, and using the
pycuda module to directly run kernels on PyCA objects.  Both are shown
here:

1) The CMakeLists.txt configuration can be used to compile the UINTA
   kernel (in the 'Kernel' directory) into a PyCAUINTA module which
   can operate on PyCA objects. This is shown in the
   Python/RunPyCAUINTA.py file.

2) The same kernel can be compiled 'on the fly' by pycuda and run on
   PyCA objects without any precompilation.  This is shown in the
   Python/RunPyCAUINTA_PyCUDA.py file

Note that only GPU implementations are given here -- no corresponding
CPU implementations are included.

For option (1), you will need to specify the location of the PyCA bin
director as the CMake variable PyCA_DIR.
