%module PyCAUINTA

%import "Core.i"

%feature("autodoc","1");

%{
#include "mem.h"
#include "pycaConst.h"
#include "Vec3D.h"
#include "Image3D.h"
#include "Field3D.h"
#include "MemoryManager.h"
#include "PyCAUINTA.h"
%}

/**
 * wrap every swig-wrapped c++ function in a try-catch block that
 * catches a PyCaException and raises a python error
 */
%exception {
  try {
    $action
  } catch (PyCA::PyCAException &awe) {
    std::string errmsg("$decl:\n");
    errmsg += awe.what();
    PyErr_SetString(PyExc_RuntimeError, const_cast<char*>(errmsg.c_str()));
    return NULL;
  }
}

%include "PyCAUINTA.h"


